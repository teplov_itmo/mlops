FROM python:3.10-alpine
RUN apk add --no-cache make
WORKDIR /app
COPY requirements.txt .
RUN pip install --upgrade pip && pip install -r requirements.txt
RUN	rm -f requirements.txt
