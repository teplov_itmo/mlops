import pandas as pd
import sys


def prepare_data(input_path, output_path):
    print("1")
    data = pd.read_csv(input_path)
    data = data[
        [
            "Pclass",
            "Sex",
            "Age",
            "SibSp",
            "Parch",
            "Fare",
            "Embarked",
            "Survived",
        ]
    ]
    data.dropna(inplace=True)
    data["Sex"] = data["Sex"].map({"male": 0, "female": 1})
    data = pd.get_dummies(data, columns=["Embarked"], drop_first=True)
    data.to_csv(output_path, index=False)


if __name__ == "__main__":
    input_path = sys.argv[1]
    output_path = sys.argv[2]
    prepare_data(input_path, output_path)
