import boto3
import os
from botocore.exceptions import (
    NoCredentialsError,
    PartialCredentialsError,
    ClientError,
)

# Set the MinIO credentials and endpoint URL
os.environ["AWS_ACCESS_KEY_ID"] = "minioadmin"
os.environ["AWS_SECRET_ACCESS_KEY"] = "minioadmin"


def list_s3_bucket_contents(bucket_name):
    try:
        # Initialize a session using MinIO
        s3_client = boto3.client(
            "s3",
            endpoint_url="http://147.45.104.102:9000",  # Replace with your MinIO server URL
            aws_access_key_id=os.getenv("AWS_ACCESS_KEY_ID"),
            aws_secret_access_key=os.getenv("AWS_SECRET_ACCESS_KEY"),
        )

        # List objects within the specified bucket
        response = s3_client.list_objects_v2(Bucket=bucket_name)

        if "Contents" in response:
            for item in response["Contents"]:
                print(item["Key"])
        else:
            print("No objects found in the bucket.")

    except NoCredentialsError:
        print("Credentials not available.")
    except PartialCredentialsError:
        print("Incomplete credentials provided.")
    except ClientError as e:
        print(f"Client error: {e}")
    except Exception as e:
        print(f"An error occurred: {e}")


# Example usage
bucket_name = "arts"  # Replace with your bucket name
list_s3_bucket_contents(bucket_name)
