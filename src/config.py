# Импортируем Path из модуля pathlib для работы с путями
from pathlib import Path

# Импортируем load_dotenv из модуля dotenv для загрузки переменных окружения
from dotenv import load_dotenv

# Импортируем logger из модуля loguru для логирования
from loguru import logger


# Загружаем переменные окружения из файла .env, если он существует
load_dotenv()

# Пути
# Определяем корневую директорию проекта
PROJ_ROOT = Path(__file__).resolve().parents[1]
# Логируем путь к корневой директории
logger.info(f"PROJ_ROOT path is: {PROJ_ROOT}")

# Определяем пути к различным директориям данных
# Директория данных
DATA_DIR = PROJ_ROOT / "data"
# Директория для сырых данных
RAW_DATA_DIR = DATA_DIR / "raw"
# Директория для промежуточных данных
INTERIM_DATA_DIR = DATA_DIR / "interim"
# Директория для обработанных данных
PROCESSED_DATA_DIR = DATA_DIR / "processed"
# Директория для внешних данных
EXTERNAL_DATA_DIR = DATA_DIR / "external"

# Определяем путь к директории моделей
MODELS_DIR = PROJ_ROOT / "models"

# Определяем пути к директориям для отчетов и фигур
# Директория отчетов
REPORTS_DIR = PROJ_ROOT / "reports"
# Директория фигур в отчетах
FIGURES_DIR = REPORTS_DIR / "figures"

# Если модуль tqdm установлен, настраиваем loguru для использования tqdm.write
# Это нужно для корректного отображения логов при использовании tqdm
try:
    from tqdm import tqdm  # Импортируем tqdm из модуля tqdm

    logger.remove(0)  # Удаляем стандартный обработчик логов
    # Добавляем новый обработчик логов с использованием tqdm.write
    logger.add(lambda msg: tqdm.write(msg, end=""), colorize=True)
except ModuleNotFoundError:  # Если модуль tqdm не найден, ничего не делаем
    pass
