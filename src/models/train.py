import click
import pandas as pd
import joblib as jb
import lightgbm as lgb
import json
import yaml

from sklearn.metrics import mean_absolute_error, mean_squared_error
from sklearn.model_selection import train_test_split

import mlflow
from mlflow.models.signature import infer_signature
import os
from dotenv import load_dotenv
from mlflow.tracking import MlflowClient

# Load environment variables from a .env file if it exists
load_dotenv()

# Set MLflow tracking URI and MinIO S3 settings
mlflow.set_tracking_uri("http://147.45.104.102:5000")
os.environ["AWS_ACCESS_KEY_ID"] = "minioadmin"
os.environ["AWS_SECRET_ACCESS_KEY"] = "minioadmin"
os.environ["MLFLOW_S3_ENDPOINT_URL"] = "http://147.45.104.102:9000"
os.environ["AWS_S3_BUCKET"] = "arts"

FEATURES = ["Pclass", "Sex", "Age", "SibSp", "Parch", "Fare", "Embarked"]


@click.command()
@click.argument("input_path", type=click.Path(exists=True))
@click.argument("output_path", type=click.Path(), nargs=2)
def train(input_path: str, output_path: str):
    """
    Train the model and log params, metrics and artifacts in MLflow
    :param input_path: Path to the processed Titanic dataset
    :param output_path: Paths for the model and score artifacts
    """
    with mlflow.start_run():
        mlflow.get_artifact_uri()

        # Load the processed Titanic dataset
        data = pd.read_csv(input_path)
        assert isinstance(
            data, pd.DataFrame
        ), "input must be a valid dataframe"
        print("1")
        print("Current working directory:", os.getcwd())

        # Load parameters from params.yaml
        with open("params.yml", "r") as fd:
            params = yaml.safe_load(fd)["train"]
        print("2")

        # Split the data into train and test sets
        x_train, x_holdout, y_train, y_holdout = train_test_split(
            data.drop("Survived", axis=1),
            data["Survived"],
            test_size=0.2,
            random_state=42,
        )
        print("3")

        lgb_train = lgb.Dataset(x_train, y_train)
        lgb_eval = lgb.Dataset(x_holdout, y_holdout, reference=lgb_train)
        print("4")

        # Train the model
        gbm = lgb.train(
            params,
            lgb_train,
            num_boost_round=100,
            valid_sets=lgb_eval,
        )
        print("5")

        # Save the model
        jb.dump(gbm, output_path[0])
        print("6")

        # Predict and evaluate
        y_predicted = gbm.predict(x_holdout, num_iteration=gbm.best_iteration)
        score = dict(
            mae=mean_absolute_error(y_holdout, y_predicted.round()),
            rmse=mean_squared_error(
                y_holdout, y_predicted.round(), squared=False
            ),
        )

        # Save the score
        with open(output_path[1], "w") as score_file:
            json.dump(score, score_file, indent=4)

        # Log the parameters, metrics, and model
        signature = infer_signature(x_holdout, y_predicted)

        mlflow.log_params(params)
        mlflow.log_metrics(score)
        mlflow.lightgbm.log_model(
            lgb_model=gbm,
            artifact_path="model",
            registered_model_name="titanic_lgbm",
            signature=signature,
        )

    # Retrieve and print the best run ID
    client = MlflowClient()
    experiment = mlflow.get_experiment_by_name("Default")
    experiment_id = experiment.experiment_id
    df = mlflow.search_runs([experiment_id])
    best_run_id = df.loc[0, "run_id"]
    print(best_run_id)


if __name__ == "__main__":
    train()
